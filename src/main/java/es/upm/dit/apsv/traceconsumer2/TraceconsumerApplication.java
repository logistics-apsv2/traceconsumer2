package es.upm.dit.apsv.traceconsumer2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import es.upm.dit.apsv.traceconsumer2.repository.TransportationOrderRepository;
import es.upm.dit.apsv.traceconsumer2.model.Trace;
import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;
import es.upm.dit.apsv.traceconsumer2.repository.TraceRepository;
import java.util.function.Consumer;

import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class TraceconsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TraceconsumerApplication.class, args);
	}
	public static final Logger log = LoggerFactory.getLogger(TraceconsumerApplication.class);
	@Autowired
	private  TraceRepository tr;
	@Autowired
	private  TransportationOrderRepository tor;	
	@Autowired
	private  Environment env;	
	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
			return t -> {
				t.setTraceId(t.getTruck() + t.getLastSeen());
				tr.save(t);
				TransportationOrder result = tor.findById(t.getTruck()).orElse(new TransportationOrder());
				if (result != null && result.getTruck()!= null
							&& ! result.getTruck().equals("")
							&& result.getSt() == 0) {
						result.setLastDate(t.getLastSeen());
						result.setLastLat(t.getLat());
						result.setLastLong(t.getLng());
						if (result.distanceToDestination() < 10)
								result.setSt(1);
						tor.save(result);
						log.info("Order updated: "+ result);
				}
		 };
	}
}
